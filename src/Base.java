public class Base {
    public float n;

    public void f(int g) {
        this.n = g;
    }
}

class L extends Base { // inheritance (mostenire)
    private long t;

    //association with M so from Base we get access to an object from class M (the object is not specified in uml so
    // i didnt write it here - but i hope the explanation confirms
    public void f() {


    }
}

class X {
    public void i(L l) {
//dependancy with class L
    }
}

class M {

}

class A {
    public void metA() {
        //compozition
        M m = new M();
    }
}

class B {
    //aggregation
    M n = new M();

    public void metB() {
    }
}