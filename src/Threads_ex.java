
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Threads_ex extends Thread {

    Threads_ex(String name) {
        super(name);
    }

    public void run() {
        for (int i = 0; i < 6; i++) {

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            //afisez ora
            System.out.println(getName() + " - " + dtf.format(now));
            try {
                //intarziere o secunda
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " Finished running.");
    }

    //i got that each thread will display 6 messages, in one second interval so there it will be 6 sets of 3 messages
    // (because we have 3 threads running on
    public static void main(String[] args) {
        Threads_ex KThread1 = new Threads_ex("Kthread1");
        Threads_ex KThread2 = new Threads_ex("Kthread2");
        Threads_ex KThread3 = new Threads_ex("Kthread3");

        KThread1.start();
        KThread2.start();
        KThread3.start();
    }
}




